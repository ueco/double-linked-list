#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct Node Node;
typedef struct Dllist Dllist;

namespace dllist
{

struct Node
{
    Node* prev;
    Node* next;
    const char* value;
};

Node* Node_new(const char* string)
{
    Node* n = (Node*)malloc(sizeof(Node));
    n->prev = NULL;
    n->next = NULL;
    n->value = string;
    return n;
}

void insertAfter(Node* node, Node* newNode)
{
    if (node == NULL || newNode == NULL)
    {
        return;
    }

    newNode->next = node->next;
    newNode->prev = node;
    if (node->next != NULL)
    {
        node->next->prev = newNode;
    }
    node->next = newNode;
}

void insertBefore(Node* node, Node* newNode)
{
    if (node == NULL || newNode == NULL)
    {
        return;
    }

    insertAfter(node->prev, newNode);
}

struct Dllist
{
    Node* begin;
    Node* end;
};

Dllist* Dllist_new()
{
    Dllist* d = (Dllist*)malloc(sizeof(Dllist));
    d->begin = NULL;
    d->end = NULL;
    return d;
}

void insertEnd(Dllist* list, Node* node)
{
    if (list == NULL || node == NULL)
    {
        return;
    }

    if (list->end == NULL)
    {
        list->begin = node;
    }
    else
    {
        insertAfter(list->end, node);
    }
    list->end = node;
}
void remove(Dllist* list, Node* node)
{
    if (list == NULL || node == NULL)
    {
        return;
    }

    if (node == list->begin && node == list->end)
    {
        list->begin = NULL;
        list->end = NULL;
    }
    else if (node == list->begin)
    {
        list->begin = node->next;
    }
    else if (node == list->end)
    {
        list->end = node->prev;
    }
    else
    {
        node->next->prev = node->prev;
        node->prev->next = node->next;
    }
    free(node);
}

Node* find(Dllist* list, const char* value)
{
    if (list == NULL || list->begin == NULL || list->end == NULL)
    {
        return NULL;
    }

    Node* node = NULL;
    do
    {
        if (node == NULL)
        {
            node = list->begin;
        }
        else
        {
            node = node->next;
        }
        if(strcmp(node->value, value) == 0)
        {
            return node;
        }
    } while (list->end != node);

    return NULL;
}

} // namespace dlllist
