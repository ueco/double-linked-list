#include <gtest/gtest.h>
#include <stdlib.h>

#include "double_linked_list.c"

class DoubleLinkedListTest : public testing::Test
{
protected:
    void SetUp()
    {
        list = dllist::Dllist_new();
    }

    void TearDown()
    {
        free(list);
    }

    dllist::Dllist* list;
};

TEST_F(DoubleLinkedListTest, null_list_node)
{
    const char* testString = "testString\n";
    dllist::Node* node = dllist::Node_new(testString);

    dllist::insertEnd(list, nullptr);
    dllist::insertEnd(nullptr, node);
    dllist::insertEnd(nullptr, nullptr);

    dllist::insertAfter(nullptr, node);
    dllist::insertAfter(node, nullptr);
    dllist::insertAfter(nullptr, nullptr);

    dllist::insertBefore(nullptr, node);
    dllist::insertBefore(node, nullptr);
    dllist::insertBefore(nullptr, nullptr);

    dllist::remove(nullptr, node);
    dllist::remove(list, nullptr);
    dllist::remove(nullptr, nullptr);

    dllist::find(nullptr, testString);
    dllist::find(list, nullptr);
    dllist::find(nullptr, nullptr);

    free(node);
}

TEST_F(DoubleLinkedListTest, add_remove_one_node)
{
    const char* testString = "testString\n";
    dllist::Node* node = dllist::Node_new(testString);

    dllist::insertEnd(list, node);
    EXPECT_EQ(list->begin, node);
    EXPECT_EQ(list->end, node);
    EXPECT_EQ(list->end->value, testString);

    dllist::remove(list, node);
    EXPECT_EQ(list->begin, nullptr);
    EXPECT_EQ(list->end, nullptr);
}

TEST_F(DoubleLinkedListTest, add_remove_nodes_next_prev)
{
    const char* testString = "testString\n";
    dllist::Node* node = dllist::Node_new(testString);
    dllist::Node* node2 = dllist::Node_new(testString);
    dllist::Node* node3 = dllist::Node_new(testString);

    dllist::insertEnd(list, node);
    dllist::insertEnd(list, node2);
    dllist::insertEnd(list, node3);

    EXPECT_EQ(list->begin, list->begin->next->prev);
    EXPECT_EQ(list->begin->next, list->begin->next->next->prev);
    EXPECT_EQ(list->begin->next->next, list->end);
    EXPECT_EQ(list->begin->next, list->end->prev);
    EXPECT_EQ(list->begin, list->end->prev->prev);

    dllist::remove(list, node);
    dllist::remove(list, node2);
    dllist::remove(list, node3);
}

TEST_F(DoubleLinkedListTest, add_nodes_remove_middle)
{
    const char* testString = "testString\n";
    dllist::Node* node = dllist::Node_new(testString);
    dllist::Node* node2 = dllist::Node_new(testString);
    dllist::Node* node3 = dllist::Node_new(testString);

    dllist::insertEnd(list, node);
    dllist::insertEnd(list, node2);
    dllist::insertEnd(list, node3);

    dllist::remove(list, node2);
    EXPECT_EQ(list->begin, node);
    EXPECT_EQ(list->end, node3);
    EXPECT_EQ(node->prev, nullptr);
    EXPECT_EQ(node->next, node3);
    EXPECT_EQ(node3->prev, node);
    EXPECT_EQ(node3->next, nullptr);

    dllist::remove(list, node);
    dllist::remove(list, node3);
}

TEST_F(DoubleLinkedListTest, add_remove_nodes_insertEnd)
{
    const char* testString = "testString\n";
    dllist::Node* node = dllist::Node_new(testString);
    dllist::Node* node2 = dllist::Node_new(testString);
    dllist::Node* node3 = dllist::Node_new(testString);
    dllist::Node* node4 = dllist::Node_new(testString);

    dllist::insertEnd(list, node);
    dllist::insertEnd(list, node2);
    EXPECT_EQ(list->begin, node);
    EXPECT_EQ(list->end, node2);

    dllist::insertEnd(list, node3);
    EXPECT_EQ(list->begin, node);
    EXPECT_EQ(list->end, node3);

    dllist::remove(list, node);
    EXPECT_EQ(list->begin, node2);
    EXPECT_EQ(list->end, node3);

    dllist::remove(list, node3);
    EXPECT_EQ(list->begin, node2);
    EXPECT_EQ(list->end, node2);

    dllist::remove(list, node2);
    EXPECT_EQ(list->begin, nullptr);
    EXPECT_EQ(list->end, nullptr);
}

TEST_F(DoubleLinkedListTest, find_node_in_list)
{
    const char* testString = "testString\n";
    dllist::Node* node = dllist::Node_new(testString);
    const char* testString2 = "testString2\n";
    dllist::Node* node2 = dllist::Node_new(testString2);
    const char* testString3 = "testString3\n";
    dllist::Node* node3 = dllist::Node_new(testString3);
    const char* testString4 = "invalidString\n";

    dllist::insertEnd(list, node);
    dllist::insertEnd(list, node2);
    dllist::insertEnd(list, node3);

    EXPECT_EQ(dllist::find(list, testString2), node2);
    EXPECT_EQ(dllist::find(list, testString), node);
    EXPECT_EQ(dllist::find(list, testString3), node3);
    EXPECT_EQ(dllist::find(list, testString4), nullptr);

    dllist::remove(list, node);
    dllist::remove(list, node2);
    dllist::remove(list, node3);
}

GTEST_API_ int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
